#!/usr/bin/env bash

FILE=$1
N=$2

for (( c=1; c<=$N; c++ ))
do
    RANDO=$((${RANDOM} % `wc -l < ${FILE}` + 1))
    RANDO2=$(shuf -i $(echo $RANDO+1 | bc)-$(echo $RANDO+500 | bc) -n 1)

    LINE=$(head -$RANDO ${FILE} | tail -1)
    LINE2=$(head -$RANDO2 ${FILE} | tail -1)

    AIRPORT=$(echo $LINE | cut -d ',' -f 3 | tr -d '"')
    AIRPORT2=$(echo $LINE2 | cut -d ',' -f 5 | tr -d '"')

    curl -s http://localhost:3000/get-flight/$AIRPORT/$AIRPORT2 &
done
