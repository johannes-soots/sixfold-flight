import Loki from "lokijs"
import {LokiMemoryAdapter} from "lokijs"

export function getTestDb(): Loki {
    const adapter = new LokiMemoryAdapter()
    const db = new Loki("test.db", {
        adapter: adapter,
    })
    db.addCollection("airport")
    db.addCollection("flight")

    return db
}

