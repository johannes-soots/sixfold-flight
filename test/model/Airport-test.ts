import {expect} from "chai"
import Airport from "../../src/model/Airport"

describe("constructor", () => {
    it("throws error if iata code is incorrect length", () => {
        expect(() => {
            new Airport({
                id: 0,
                name: "Tallinn",
                city: "TTT",
                country: "EEE",
                iata: "ABCDE",
                icao: "DSFF",
                latitude: 59.432742,
                longitude: 24.762984
            })
        }).to.throw()
    })

    it("throws error if icao code is incorrect length", () => {
        expect(() => {
            new Airport({
                id: 0,
                name: "Tallinn",
                city: "TTT",
                country: "EEE",
                iata: "OOO",
                icao: "ABCDE",
                latitude: 59.432742,
                longitude: 24.762984
            })
        }).to.throw()
    })
})
