import {expect} from "chai"
import Flight from "../../src/model/Flight"
import Airport from "../../src/model/Airport"

describe("calculateDistance", () => {
    it("returns ~82.55 for distance between points in Tallinn and Helsniki", () => {
        const airport1 = new Airport({
            id: 0,
            name: "Tallinn",
            city: "TTT",
            country: "EEE",
            iata: "DSF",
            icao: "DSFF",
            latitude: 59.432742,
            longitude: 24.762984
        })

        const airport2 = new Airport({
            id: 0,
            name: "Helsinki",
            city: "HHH",
            country: "FFF",
            iata: "SDF",
            icao: "SDFG",
            latitude: 60.169866,
            longitude: 24.938472
        })
        expect(Flight.calculateDistance(airport1, airport2)).to.eq(82.54996405182263)
    })

    it("returns ~16 359.72 for distance between points in Anchorage and Concordia station", () => {
        const airport1 = new Airport({
            id: 0,
            name: "Concordia",
            city: "TTT",
            country: "EEE",
            iata: "DSF",
            icao: "DSFF",
            latitude: -75.099340,
            longitude: 123.341188
        })

        const airport2 = new Airport({
            id: 0,
            name: "Anchorage",
            city: "HHH",
            country: "FFF",
            iata: "SDF",
            icao: "SDFG",
            latitude: 61.176825,
            longitude: -149.793883
        })
        expect(Flight.calculateDistance(airport1, airport2)).to.eq(16359.718703774197)
    })

})
