import {expect} from "chai"
import buildAdjacencyList from "../../src/operations/build-adjacency-list"
import {getTestDb} from "../test-helpers"

describe("buildAdjacencyList", () => {
    it("builds adjacency list", () => {
        const db = getTestDb()
        const airports = db.getCollection("airport")
        const flights = db.getCollection("flight")

        airports.insert([{id: 1}, {id: 2}, {id: 3}, {id: 4}])
        flights.insert([{from: 1, to: 2}, {from: 3, to: 1}, {from: 2, to: 4}, {from: 1, to: 3}])

        const adjacencyList = buildAdjacencyList(db)

        expect(adjacencyList[1][0].from).to.eq(1)
        expect(adjacencyList[1][0].to).to.eq(2)
        expect(adjacencyList[1][1].to).to.eq(3)

        expect(adjacencyList[2][0].from).to.eq(2)
        expect(adjacencyList[2][0].to).to.eq(4)

        expect(adjacencyList[3][0].from).to.eq(3)
        expect(adjacencyList[3][0].to).to.eq(1)
    })

})
