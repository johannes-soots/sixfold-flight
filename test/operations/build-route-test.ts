import { expect } from "chai"
import Flight from "../../src/model/Flight"
import Airport from "../../src/model/Airport"
import buildAdjacencyList from "../../src/operations/build-adjacency-list"
import buildRoute from "../../src/operations/build-route"
import { getTestDb } from "../test-helpers"

describe("buildRoute", () => {
    let db: Loki

    before(() => {
        db = getTestDb()

        db.deleteDatabase()
        const airports = db.getCollection("airport")
        const flights = db.getCollection("flight")

        airports.insert([
            {
                id: 1,
                name: "Tallinn",
                city: "TTT",
                country: "EEE",
                iata: "ABC",
                icao: "DSFF",
                latitude: 0,
                longitude: 0,
            },
            {
                id: 2,
                name: "Tartu",
                city: "TAT",
                country: "EEE",
                iata: "TAT",
                icao: "TATA",
                latitude: 0,
                longitude: 0,
            },
            {
                id: 3,
                name: "Narva",
                city: "NAN",
                country: "EEE",
                iata: "NAN",
                icao: "NANA",
                latitude: 0,
                longitude: 0,
            },
            {
                id: 4,
                name: "Pärnu",
                city: "PAP",
                country: "EEE",
                iata: "PAP",
                icao: "PAPA",
                latitude: 0,
                longitude: 0,
            },
            {
                id: 5,
                name: "Kuressaare",
                city: "KUK",
                country: "EEE",
                iata: "KUK",
                icao: "KUKU",
                latitude: 0,
                longitude: 0,
            },
            {
                id: 6,
                name: "Võru",
                city: "VOV",
                country: "EEE",
                iata: "VOV",
                icao: "VOVO",
                latitude: 0,
                longitude: 0,
            },
            {
                id: 7,
                name: "Viljandi",
                city: "VIV",
                country: "EEE",
                iata: "VIV",
                icao: "VIVI",
                latitude: 0,
                longitude: 0,
            },
        ])
        flights.insert([
            { from: 1, to: 5, distance: 10 },
            { from: 1, to: 4, distance: 4 },
            { from: 2, to: 1, distance: 3 },
            { from: 2, to: 3, distance: 1 },
            { from: 3, to: 5, distance: 2 },
            { from: 4, to: 1, distance: 4 },
            { from: 4, to: 3, distance: 12 },
            { from: 5, to: 2, distance: 2 },
            { from: 5, to: 4, distance: 6 },

            { from: 3, to: 6, distance: 1 },
            { from: 6, to: 7, distance: 1 },
        ])
    })

    it("finds shortest route between 1 and 3 through 5, 2 and 3", () => {
        const fromAirport = Airport.get(1, db)
        const toAirport = Airport.get(3, db)
        const adjacencyList = buildAdjacencyList(db)
        const route = buildRoute(fromAirport, toAirport, db, adjacencyList)
        const expected = {
            route: [
                { to: 5, distance: 10, stops: 1, from: 1 },
                { to: 2, distance: 12, stops: 2, from: 5 },
                { to: 3, distance: 13, stops: 3, from: 2 },
            ],
            distance: 13,
            stops: 3,
        }

        expect(route).to.deep.eq(expected)
    })

    it("throws if can't find path between airports", () => {
        const airports = db.getCollection("airport")
        airports.insertOne({
            id: 0,
            name: "NaN",
            city: "NAN",
            country: "NAN",
            iata: "NAN",
            icao: "NANN",
            latitude: 0,
            longitude: 0,
        })

        const fromAirport = Airport.get(1, db)
        const toAirport = Airport.get(0, db)
        const adjacencyList = buildAdjacencyList(db)

        expect(() =>
            buildRoute(fromAirport, toAirport, db, adjacencyList)
        ).to.throw("Can't find path between Tallinn to NaN")
    })

    it("returns empty path if from and to airports are same", () => {
        const fromAirport = Airport.get(1, db)
        const toAirport = Airport.get(1, db)
        const adjacencyList = buildAdjacencyList(db)

        expect(
            buildRoute(fromAirport, toAirport, db, adjacencyList)
        ).to.deep.eq({ route: [], distance: 0, stops: 0 })
    })

    it("finds path from 3 to 1 through 5 and 2", () => {
        const fromAirport = Airport.get(3, db)
        const toAirport = Airport.get(1, db)
        const adjacencyList = buildAdjacencyList(db)

        const route = buildRoute(fromAirport, toAirport, db, adjacencyList)
        const expected = {
            route: [
                { to: 5, distance: 2, stops: 1, from: 3 },
                { to: 2, distance: 4, stops: 2, from: 5 },
                { to: 1, distance: 7, stops: 3, from: 2 },
            ],
            distance: 7,
            stops: 3,
        }

        expect(route).to.deep.eq(expected)
    })


    it("switches to longer route if shortest is more then 4 stops", () => {
        const fromAirport = Airport.get(1, db)
        const toAirport = Airport.get(7, db)
        const adjacencyList = buildAdjacencyList(db)

        const route = buildRoute(fromAirport, toAirport, db, adjacencyList)
        const expected = {
            route: [
                { to: 4, distance: 4, stops: 1, from: 1 },
                { to: 3, distance: 16, stops: 2, from: 4 },
                { to: 6, distance: 17, stops: 3, from: 3 },
                { to: 7, distance: 18, stops: 4, from: 6 },
            ],
            distance: 18,
            stops: 4,
        }
        expect(route).to.deep.eq(expected)
    })
})
