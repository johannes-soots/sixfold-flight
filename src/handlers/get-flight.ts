import { Request, Response } from "express"
import buildRoute from "../operations/build-route"
import Airport from "../model/Airport"
import Flight from "../model/Flight"

export default function getFlight(req: Request, res: Response) {
    const { from, to } = req.params
    const { db, adjacencyList } = req.app.locals

    try {
        const fromAirport = Airport.getByCode(from, db)
        const toAirport = Airport.getByCode(to, db)
        const { route, distance, stops } = buildRoute(
            fromAirport,
            toAirport,
            db,
            adjacencyList
        )
        const remapped = route.map(flight => {
            if (!flight.from) {
                throw "Missing source airport ID from flight"
            }

            return new Flight(flight.from, flight.to, db)
        })

        res.status(200).send({
            from: fromAirport,
            to: toAirport,
            distance: distance,
            stops: stops,
            route: remapped,
        })
    } catch (e) {
        console.error(e)

        res.status(500).send({
            error: e,
        })
    }
}
