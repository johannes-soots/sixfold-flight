import Airport from "./Airport"
import Loki from "lokijs"

export interface FlightParams {
    from: number
    to: number
    distance: number
}

export default class Flight {
    public from: Airport
    public to: Airport
    public distance: number

    constructor(from: number, to: number, db: Loki, distance?: number) {
        this.from = Airport.get(from, db)
        this.to = Airport.get(to, db)
        this.distance = distance || Flight.calculateDistance(this.from, this.to)
    }

    public static findFrom(from: number, db: Loki): FlightParams[] {
        const flights = this.getCollection(db)

        return flights.find({ from: from })
    }

    public toDbFormat() {
        return {
            from: this.from.id,
            to: this.to.id,
            distance: this.distance,
        }
    }

    public static getCollection(db: Loki) {
        return db.getCollection("flight")
    }

    public static calculateDistance(
        airport1: Airport,
        airport2: Airport
    ): number {
        let R = 6371e3
        let φ1 = this.toRadians(airport1.latitude)
        let φ2 = this.toRadians(airport2.latitude)
        let Δφ = this.toRadians(airport2.latitude - airport1.latitude)
        let Δλ = this.toRadians(airport2.longitude - airport1.longitude)

        let a =
            Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
            Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2)
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))

        let d = R * c

        return d / 1000
    }

    private static toRadians(degrees: number) {
        return (degrees * Math.PI) / 180
    }
}
