import Loki from "lokijs"

interface AirportParams {
    id: number
    name: string
    city: string
    country: string
    iata: string | null
    icao: string | null
    latitude: number
    longitude: number
}

export default class Airport {
    public id: number
    public name: string
    public city: string
    public country: string
    public iata: string | null
    public icao: string | null
    public latitude: number
    public longitude: number

    constructor(params: AirportParams) {
        Airport.validateIcaoandIata(params)

        this.id = params.id
        this.name = params.name
        this.city = params.city
        this.country = params.country
        this.iata = params.iata
        this.icao = params.icao
        this.latitude = params.latitude
        this.longitude = params.longitude
    }

    public static get(id: number, db: Loki): Airport {
        const airports = this.getCollection(db)
        const airport = airports.findOne({ id: id })

        if (!airport) {
            throw `Airport ${id} not found!`
        }

        return new Airport(airport)
    }

    public static getByCode(code: string, db: Loki): Airport {
        let airport: AirportParams

        const airports = this.getCollection(db)

        if (code.length === 3) {
            airport = airports.findOne({ iata: code })
        } else if (code.length === 4) {
            airport = airports.findOne({ icao: code })
        } else {
            throw "Invalid code"
        }

        if (airport === null) {
            throw `Airport ${code} not found!`
        }

        return new Airport(airport)
    }

    public static getIds(db: Loki): number[] {
        return Airport.getCollection(db).extract("id")
    }

    public static getCollection(db: Loki): Loki.Collection {
        return db.getCollection("airport")
    }

    private static validateIcaoandIata({
        icao: icao,
        iata: iata,
    }: AirportParams) {
        if (!icao && !iata) {
            throw "Both ICAO and IATA codes are missing, invalid airport"
        }
        if (icao && icao.length !== 4) {
            throw `Invalid ICAO code: ${icao}`
        }
        if (iata && iata.length !== 3) {
            throw `Invalid IATA code: ${iata}`
        }
    }
}
