import express from "express"
import * as bodyParser from "body-parser"
import getFlight from "./handlers/get-flight"
import loadAirports from "./operations/load-airports"
import loadFlights from "./operations/load-routes"
import buildAdjacencyList from "./operations/build-adjacency-list"
import Lokijs from "lokijs"
import { LokiFsAdapter } from "lokijs"
import Flight from "./model/Flight"
import Airport from "./model/Airport"

const PORT = 3000

export default class App {
    public app: express.Application
    public db: Lokijs

    constructor() {
        this.app = express()
        this.initRoutes()
        this.config()
        this.db = this.initDb()
        this.app.locals.db = this.db
    }

    private config(): void {
        this.app.use(bodyParser.json())
        this.app.use(bodyParser.urlencoded({ extended: false }))
    }

    private initRoutes(): void {
        const router = express.Router()

        router.get("/get-flight/:from/:to", getFlight)

        this.app.use(router)
    }

    private initDb(): Loki {
        const adapter = new LokiFsAdapter()
        return new Lokijs("sixfold-flight.db", {
            adapter: adapter,
            autosave: true,
            autoload: true,
            verbose: true,
            autoloadCallback: () => App.startApp(this.app, this.db),
        })
    }

    private static async loadData(db: Lokijs): Promise<void> {
        return new Promise((resolve, reject) => {
            if (Airport.getCollection(db) === null) {
                console.log("Loading airports")
                loadAirports(db)
            }
            if (Flight.getCollection(db) === null) {
                console.log("Loading flights")
                loadFlights(db)
            }
            db.saveDatabase(err => {
                if (err) {
                    reject(err)
                }
                console.log("Database is ready!")
                resolve()
            })
        })
    }

    public static async startApp(app: express.Application, db: Loki) {
        await App.loadData(db)
        app.locals.adjacencyList = buildAdjacencyList(db)

        app.listen(PORT, () => {
            console.log("Express server listening on port " + PORT)
        })
    }
}
