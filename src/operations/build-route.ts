import Airport from "../model/Airport"
import Loki from "lokijs"
import cloneDeep from "lodash/cloneDeep"
import { AdjacencyList } from "./build-adjacency-list"

const MAX_STOPS = 4

interface Flight {
    to: number
    from?: number
    distance: number
    stops: number
}

interface SolvedList {
    [id: number]: Flight
}

interface QueueAirport {
    id: number
    distance: number
}

export default function buildRoute(
    fromAirport: Airport,
    toAirport: Airport,
    db: Loki,
    airports: AdjacencyList
): {
    route: Flight[]
    distance: number
    stops: number
} {
    let adjacencyList = cloneDeep(airports)
    const { solved, queue } = initializeValues(fromAirport, db)

    while (queue.length) {
        const shortestFlight: QueueAirport | undefined = getShortestDist(queue)

        if (
            shortestFlight === undefined ||
            shortestFlight.distance === Number.POSITIVE_INFINITY
        ) {
            throw `Can't find path between ${fromAirport.name} to ${toAirport.name}`
        }

        if (shortestFlight.id === toAirport.id) {
            break
        }

        if (adjacencyList[shortestFlight.id]) {
            adjacencyList[shortestFlight.id].forEach(flight => {
                const alt = solved[shortestFlight.id].distance + flight.distance
                const stops = solved[shortestFlight.id].stops + 1

                if (alt < solved[flight.to].distance && stops <= MAX_STOPS) {
                    const toId = queue.findIndex(q => q.id === flight.to)

                    solved[flight.to].distance = alt
                    solved[flight.to].from = flight.from
                    solved[flight.to].stops = stops
                    queue[toId].distance = alt
                }
            })
        }
    }

    return {
        route: buildPath(solved, fromAirport, toAirport),
        distance: solved[toAirport.id].distance,
        stops: solved[toAirport.id].stops,
    }
}

function getShortestDist(queue: QueueAirport[]) {
    queue.sort((a, b) => {
        return b.distance - a.distance
    })

    return queue.pop()
}

function initializeValues(
    fromAirport: Airport,
    db: Loki
): { solved: SolvedList; queue: QueueAirport[] } {
    const airportIds = Airport.getIds(db)
    const solved: SolvedList = {}
    const queue: QueueAirport[] = []

    airportIds.forEach(id => {
        if (id === fromAirport.id) {
            solved[id] = {
                to: fromAirport.id,
                distance: 0,
                stops: 0,
            }

            queue.push({ id: id, distance: 0})
        } else {
            solved[id] = {
                to: id,
                distance: Number.POSITIVE_INFINITY,
                stops: 0,
            }

            queue.push({ id: id, distance: Number.POSITIVE_INFINITY})
        }
    })

    return {
        solved: solved,
        queue: queue,
    }
}

function buildPath(
    solved: SolvedList,
    origin: Airport,
    dest: Airport
): Flight[] {
    let start = dest.id
    const path: Flight[] = []

    while (start !== origin.id) {
        const { from } = solved[start]

        if (!from) {
            throw "Solved airport does not have from airport set!"
        }

        path.push(solved[start])
        start = from
    }

    return path.reverse()
}
