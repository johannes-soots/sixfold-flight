import * as fs from "fs"
import parse from "csv-parse/lib/sync"
import Loki from "lokijs"
import Flight from "../model/Flight"

interface FlightParams {
    source_airport_id: string
    dest_airport_id: string
}

const columns = [
    "airline",
    "airline_id",
    "source_airport",
    "source_airport_id",
    "dest_airport",
    "dest_airport_id",
    "codeshare",
    "stops",
    "equipment",
]

export default function loadRoutes(db: Loki): void {
    const file = fs.readFileSync("routes.csv").toString()
    const parsed = parse(file, { columns: columns })
    const flights = mapFlights(parsed, db)
    const flightsCollection = db.addCollection("flight")

    flightsCollection.insert(flights.map(flight => flight.toDbFormat()))
}

function mapFlights(parsedCsv: FlightParams[], db: Loki): Flight[] {
    const flights: Flight[] = []

    parsedCsv.forEach(flightParams => {
        try {
            const fromAirport = parseInt(flightParams.source_airport_id)
            const toAirport = parseInt(flightParams.dest_airport_id)
            const flight = new Flight(fromAirport, toAirport, db)

            flights.push(flight)
        } catch (e) {
            console.error(
                `Failed to create Flight because: ${e}\nWith params: ${JSON.stringify(
                    flightParams
                )}`
            )
        }
    })

    return flights
}
