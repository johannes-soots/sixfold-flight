import * as fs from "fs"
import parse from "csv-parse/lib/sync"
import Airport from "../model/Airport"
import Loki from "lokijs"

interface AirportParams {
    id: string
    name: string
    city: string
    country: string
    iata: string
    icao: string
    latitude: string
    longitude: string
}

const columns = [
    "id",
    "name",
    "city",
    "country",
    "iata",
    "icao",
    "latitude",
    "longitude",
    "altitude",
    "timezone",
    "dst",
    "tz",
    "type",
    "source",
]

export default function loadAirports(db: Loki): void {
    const file = fs.readFileSync("airports.csv").toString()
    const parsed = parse(file, { columns: columns, cast: parseNull })
    const airports = mapAirports(parsed)
    const airportsCollection = db.addCollection("airport")

    airportsCollection.insert(airports)
}

function mapAirports(parsedCsv: AirportParams[]): Airport[] {
    const airports: Airport[] = []

    parsedCsv.forEach(airport_params => {
        try {
            const airport = new Airport({
                id: parseInt(airport_params.id),
                name: airport_params.name,
                country: airport_params.country,
                city: airport_params.city,
                iata: airport_params.iata,
                icao: airport_params.icao,
                latitude: parseFloat(airport_params.latitude),
                longitude: parseFloat(airport_params.longitude),
            })

            airports.push(airport)
        } catch (e) {
            console.error(
                `Failed to create Airport because: ${e}\nWith params: ${JSON.stringify(
                    airport_params
                )}`
            )
        }
    })

    return airports
}

function parseNull(value: string): null | string {
    if (value === "\\N") {
        return null
    }

    return value
}
