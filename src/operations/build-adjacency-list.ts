import { FlightParams } from "../model/Flight"
import Airport from "../model/Airport"
import Flight from "../model/Flight"
import Loki from "lokijs"

export interface AdjacencyList {
    [id: number]: FlightParams[]
}

export default function buildAdjacencyList(db: Loki): AdjacencyList {
    const airports = Airport.getIds(db)
    const adjacencyList: AdjacencyList = {}

    airports.forEach(a => {
        const flights = Flight.findFrom(a, db)
        if (flights.length > 0) {
            adjacencyList[a] = flights
        }
    })

    return adjacencyList
}
