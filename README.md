# Build & start
I used yarn, but it works with npm too. 

#### Build

`yarn build`

#### Start

`yarn start`

First run will take longer, it builds up the airport and routes database from csv files. 
Also due to data inconsistencies there will be errors on first run.

Try the endpoint

`curl http://localhost:3000/get-flight/TLL/SVO`

Or you could use `stress-test.sh` by running `./stress-test.sh routes.csv 10` 

### Test Graph
![Test Graph](test_graph.png "Test Graph")
